package com.redlg1.cryptotracker;

import static android.os.SystemClock.sleep;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private LinearLayout b1;
    private LinearLayout b2;
    private LinearLayout b3;

    private Button button1;

    private volatile boolean state = false;

    private final Runnable daemon = () -> {
        main:while (this.state) {
            b1.setBackgroundColor(Color.RED);
            sleep(2000);
            if (!this.state) {
                b1.setBackgroundColor(Color.GRAY);
                break;
            }
            for (int i = 100; i <= 500; i += 100) {
                b1.setBackgroundColor(Color.RED);
                sleep(i);
                b1.setBackgroundColor(Color.GRAY);
                sleep(i);
                if (!this.state) {
                    b1.setBackgroundColor(Color.GRAY);
                    break main;
                }

            }
            b2.setBackgroundColor(Color.YELLOW);
            b1.setBackgroundColor(Color.GRAY);
            sleep(2000);
            if (!this.state) {
                b2.setBackgroundColor(Color.GRAY);
                break;
            }

            for (int i = 100; i <= 500; i += 100) {
                b2.setBackgroundColor(Color.YELLOW);
                sleep(i);
                b2.setBackgroundColor(Color.GRAY);
                sleep(i);
                if (!this.state) {
                    b2.setBackgroundColor(Color.GRAY);
                    break main;
                }

            }
            b3.setBackgroundColor(Color.GREEN);
            b2.setBackgroundColor(Color.GRAY);
            sleep(2000);
            if (!this.state) {
                b3.setBackgroundColor(Color.GRAY);
                break;
            }

            for (int i = 100; i <= 500; i += 100) {
                b3.setBackgroundColor(Color.GREEN);
                sleep(i);
                b3.setBackgroundColor(Color.GRAY);
                sleep(i);
                if (!this.state) {
                    b3.setBackgroundColor(Color.GRAY);
                    break main;
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = findViewById(R.id.bulb_1);
        b2 = findViewById(R.id.bulb_2);
        b3 = findViewById(R.id.bulb_3);
        button1 = findViewById(R.id.button_1);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onClickStart(View view) {
        if(!state) {
            state = true;
            new Thread(daemon).start();
            button1.setText("Stop");
        } else {
            state = false;
            button1.setText("Start");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.state = false;
        button1.setText("Start");
    }
}